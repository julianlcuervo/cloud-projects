import pytest
from circulo import Circulo


centro = [0,0]
radio = [2]
coor = [3,3]

centro2 = [0,0]
radio2 = [2]
coor2 = [1,1]



@pytest.mark.parametrize("p1",
                         [(Circulo(centro[0],centro[1],radio[0]))]
                         )


def test_distancia(p1):
    assert p1.distancia(coor[0],coor[1]) == "coordenada fuera del circulo"
    assert p1.distancia(coor2[0], coor2[1]) == "coordenada dentro del circulo"