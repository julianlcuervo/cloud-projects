import math


class Circulo:
    def __init__(self, cx, cy, radio):
        self.cx = cx
        self.cy = cy
        self.radio = radio

    def distancia(self, x2, y2):
        dcir = math.sqrt(((self.radio - self.cx) ** 2) + ((self.radio - self.cy) ** 2))
        dcoor = math.sqrt(((x2 - self.cx) ** 2) + ((y2 - self.cy) ** 2))

        if dcoor <= dcir:
            return "coordenada dentro del circulo"
        else:
            return "coordenada fuera del circulo"


centro = [0,0]
radio = [2]
coor = [1,1]
circulo = Circulo(centro[0], centro[1], radio[0])
circulo.distancia(coor[0], coor[1])