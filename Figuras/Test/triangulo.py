import math

class Triangulo:
    def __init__(self, pun1, pun2, pun3):
        self.pun1 = pun1
        self.pun2 = pun2
        self.pun3 = pun3

    def distancia(self, p1, p2):
        r = math.ceil(math.sqrt(((p2[0] - p1[0]) ** 2) + ((p2[1] - p1[1]) ** 2)))
        return r

    def tipo(self):
        d1 = t.distancia(self.pun1, self.pun2)
        d2 = t.distancia(self.pun2, self.pun3)
        d3 = t.distancia(self.pun1, self.pun3)

        if (d1 == d2 and d2 == d3):
            return "Equilatero"
        elif (d1 == d2 or d1 == d3 or d2 == d3):
            return "Isoceles"
        elif (d1 != d2 or d1 != d3 or d2 != d3):
            return "Escaleno"

pun1 = [1,0]
pun2 = [-2,2]
pun3 = [0,5]
t = Triangulo(pun1, pun2, pun3)
t.tipo()

