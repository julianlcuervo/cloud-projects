import pytest
import math
from triangulo import Triangulo


pun1 = [1,0]
pun2 = [-2,2]
pun3 = [0,5]

pun12 = [1,2]
pun22 = [9,3]
pun32 = [3,5]

pun13 = [1,1]
pun23 = [2,1]
pun33 = [1.5,(2+math.sqrt(3))/2]


@pytest.mark.parametrize("p1,res",
                         [(Triangulo(pun1,pun2,pun3),"Isoceles"),
                          (Triangulo(pun12,pun22,pun32),"Escaleno"),
                          (Triangulo(pun13,pun23,pun33),"Equilatero")]
                         )


def test_tipo(p1,res):
    assert p1.tipo() == res

